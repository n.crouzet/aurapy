# aurapy

A client library for spinning the Aura actuator.

## Getting Started

1. Install `aurapy`, the aura python package:  
`pip install aurapy --upgrade --extra-index-url https://gitlab.com/api/v4/projects/35011838/packages/pypi/simple` 

2. Connect the Aura actuator to your PC using USB.

3. Connect the power cable to the Aura actuator.

That's it - you may run some code in a python interpreter!  
Take a look at examples in `demo.py`:

```
from aurapy import demo
demo.start_stop() # spinning the motor - make sure nothing is in the way.
```

And check-out the built-in command-line interface:
```
# from a python session
import aurapy; aurapy.run()
```
```
# straight from the command line
python -m aurapy
```


## Next Steps


You can explore the AuraClient interface to see what commands are available and
drive the motor with your own script:

```
help(aurapy.AuraClient)
```

## Embedded Software Update
Here is how to update the embedded actuator software to the latest version:
```
import aurapy
aurapy.update_aura_to_latest_version()
```


## Issues

Please use [gitlab issues](https://gitlab.com/kytheralab/auraclient/-/issues) to report any abnormal behavior or ask questions.



